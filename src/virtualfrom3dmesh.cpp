
#include <iostream>     // std::cout, std::endl
#include <iomanip>      // std::setfill, std::setw

#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <sstream>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// define this if you want to see 3D polygon meshes
//#define DRAW_MESH

//
// global variables
//

using namespace std; 
using namespace cv; 

bool gInitialized = false;
GLuint gTextures[ 6];
double gValidTextureWidth;
double gValidTextureHeight;
int gCols, gRows;
double *gTable[ 6];
bool gAlphamaskAvailable = false;
double gPan = 0.0, gTilt = 0.0;
int gMouseState = GLUT_UP;
int gMouseX, gMouseY;
char *gAlphamaskFilePrefix;

Mat image[6]; // Images contained here.
Mat mask[6]; // Images contained here.

//
// helper functions
//

//JB: Save images?:S
void CaptureViewPort()
{
 
 GLubyte * bits; //RGB bits
 GLint viewport[4]; //current viewport
  
 //get current viewport
 glGetIntegerv(GL_VIEWPORT, viewport);

 int w = viewport[2];
 int h = viewport[3];
 
 bits = new GLubyte[w*3*h];

 //read pixel from frame buffer
 glFinish(); //finish all commands of OpenGL
 glPixelStorei(GL_PACK_ALIGNMENT,1); //or glPixelStorei(GL_PACK_ALIGNMENT,4);
 glPixelStorei(GL_PACK_ROW_LENGTH, 0);
 glPixelStorei(GL_PACK_SKIP_ROWS, 0);
 glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
 glReadPixels(0, 0, w, h, GL_BGR_EXT, GL_UNSIGNED_BYTE, bits);

 Mat capImg = Mat( Size(w,h), CV_8UC3);
 for(int i=0; i < h; ++i)
 {
  for(int j=0; j < w; ++j)
  {
   capImg.ptr<unsigned char>(i,j)[0] = (unsigned char)(bits[(h-i-1)*3*w + j*3+0]);
   capImg.ptr<unsigned char>(i,j)[1] = (unsigned char)(bits[(h-i-1)*3*w + j*3+1]);
   capImg.ptr<unsigned char>(i,j)[2] = (unsigned char)(bits[(h-i-1)*3*w + j*3+2]);
  }
 }

// cvSaveImage("/home/jep/result.jpg",capImg); 
// cvReleaseImage(&capImg); 

	std::vector<int> qualityType;
	qualityType.push_back(CV_IMWRITE_JPEG_QUALITY);
	qualityType.push_back(95); //Jpeg quality

	bool writing_success;
	writing_success=imwrite("/home/jep/result.jpg", capImg, qualityType);
	if(!writing_success){
		std::cout << "\nError writing image! Check output directory existence." << std::endl;
	}

 delete[] bits; 
 
}


void outputGlError( string pszLabel )
{
   GLenum errorno = glGetError();
   
   if ( errorno != GL_NO_ERROR )
   {
      printf( 
         "%s had error: #(%d) %s\r\n", 
         pszLabel.c_str(), 
         errorno, 
         gluErrorString( errorno ) );
   }
}

int get_minimum_power_of_two( int in)
{
   int i = 1;
   while ( i < in){
      i *= 2;
   }
   return i;
}

bool read_3d_mesh( char *mesh_file_path)
{
   FILE *fp = fopen( mesh_file_path, "r");
   if ( fp == NULL){
      printf( "Can't read 3D mesh file: %s\n", mesh_file_path);
      return false;
   }

   if ( fscanf( fp, "cols %d rows %d\n", &gCols, &gRows) != 2){
      printf( "Can't read cols/rows in 3d mesh file.\n");
	  fclose( fp);
      return false;
   }

   for ( int c = 0; c < 6; c++)
   {
      gTable[ c] = new double[ gCols * gRows * 3];

      for ( int iRow = 0; iRow < gRows; iRow++ )
      {
         for ( int iCol = 0; iCol < gCols; iCol++ )
         {
            double x, y, z;
            if ( fscanf( fp, "%lf, %lf, %lf", &x, &y, &z) != 3){
               printf( "Can't read grid data in 3d mesh file.\n");
               return false;
            }
            gTable[c][ ( iRow * gCols + iCol) * 3 + 0] = x;
            gTable[c][ ( iRow * gCols + iCol) * 3 + 1] = y;
            gTable[c][ ( iRow * gCols + iCol) * 3 + 2] = z;
         }
      }
   }

   fclose( fp);

   return true;
}

void initialize( void)
{   

   int width, height;

   glGenTextures( 6, gTextures);
   outputGlError( "initialize glGenTextures" );

   for ( int cam = 0; cam < 6; cam++)
   {
      glBindTexture( GL_TEXTURE_2D, gTextures[ cam]);

      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

      // Make a texture from the PPM image.
      // Texture size is set to the minimum of power of two which can contain the PPM
      // so that this program works on lower OpenGL versions.

		width=1616;
		height=1232;


      int texture_width = get_minimum_power_of_two( width);
      int texture_height = get_minimum_power_of_two( height);
      gValidTextureWidth = (double)width/texture_width;
      gValidTextureHeight = (double)height/texture_height;

      // copy PPM image pixels to the valid region of the texture.
      unsigned char *texture_buffer = new unsigned char[ texture_width * texture_height * 4];
      for ( int y = 0; y < height; y++){
         for ( int x = 0; x < width; x++){
				//cout << (int) image[cam].ptr<unsigned char>(y,x)[0] << endl;
            texture_buffer[ ( y * texture_width + x) * 4 + 0] = (unsigned char) image[cam].ptr<unsigned char>(y,x)[0]; // B
            texture_buffer[ ( y * texture_width + x) * 4 + 1] = (unsigned char) image[cam].ptr<unsigned char>(y,x)[1]; // G
            texture_buffer[ ( y * texture_width + x) * 4 + 2] = (unsigned char) image[cam].ptr<unsigned char>(y,x)[2]; // R

				if( gAlphamaskAvailable){
        	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) mask[cam].ptr<unsigned char>(y,x)[0]; 
				}
				else{
        	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) 1; // A
				}
         }
      }

      // transfer the RGB buffer to graphics card.
      glTexImage2D(
         GL_TEXTURE_2D, 
         0, 
         GL_RGBA8, 
         texture_width, 
         texture_height, 
         0, 
         GL_BGRA_EXT, 
         GL_UNSIGNED_BYTE,
         texture_buffer );
      outputGlError( "initialize glTexImage2d()" );    

      // now the RGB texture is transferred to graphics, we won't need these.
      delete []texture_buffer;

#ifdef DRAW_MESH
      glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
#else
      glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
      glEnable( GL_TEXTURE_2D );
      glShadeModel( GL_FLAT );

      if ( gAlphamaskAvailable){
         glEnable( GL_BLEND );
         glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
      }
#endif
   }
}

//
// GLUT handlers
//
void display( void)
{
   if ( !gInitialized){
      initialize();
      gInitialized = true;
   }

   glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
   glClearDepth(1.0);
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   glRotated( -gTilt, 1.0, 0.0, 0.0); // tilt the view
   glRotated( -gPan, 0.0, 1.0, 0.0); // pan the view
   glRotated( -90.0, 1.0, 0.0, 0.0); // convert Ladybug 3D coordinate system to OpenGL's one

   glMatrixMode( GL_TEXTURE );
   glLoadIdentity();

   for ( int c = 0; c < 6; c++) // for each camera
   {
      glBindTexture( GL_TEXTURE_2D, gTextures[ c]);
#ifdef DRAW_MESH
      glColor3f( (c+1)&1, ((c+1)>>1)&1, ((c+1)>>2)&1);
#endif
      for ( int iRow = 0; iRow < gRows - 1; iRow++ ) // for each row
      {
         glBegin( GL_TRIANGLE_STRIP);
         for ( int iCol = 0; iCol < gCols; iCol++ ) // for each column
         {   
            double p1 = (double)iCol / ( gCols - 1) * gValidTextureWidth;
            double q1 = (double)iRow / ( gRows - 1) * gValidTextureHeight;
            double q2 = (double)( iRow + 1.0) / ( gRows - 1) * gValidTextureHeight;

            int ptr1 = iRow * gCols + iCol;
            double x1 = gTable[c][ ptr1 * 3 + 0];
            double y1 = gTable[c][ ptr1 * 3 + 1];
            double z1 = gTable[c][ ptr1 * 3 + 2];

            int ptr2 = ( iRow + 1) * gCols + iCol;
            double x2 = gTable[c][ ptr2 * 3 + 0];
            double y2 = gTable[c][ ptr2 * 3 + 1];
            double z2 = gTable[c][ ptr2 * 3 + 2];

            glTexCoord2d( p1, q1);
            glVertex3d( x1, y1, z1);
                              
            glTexCoord2d( p1, q2);
            glVertex3d( x2, y2, z2);
         }
         glEnd();
      }      
   }

   glutSwapBuffers();
}

void idle( void)
{
   glutPostRedisplay();
}

void resize( int w, int h )
{
   glViewport(0, 0, w, h);
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 60.0, (GLfloat)w/(GLfloat)h, 0.1, 100.0 );
}

void mouse( int button, int state, int x, int y )
{
   gMouseState = state;
   if ( gMouseState == GLUT_DOWN){
      gMouseX = x;
      gMouseY = y;
   }
}

void motion( int x, int y )
{
   if ( gMouseState == GLUT_DOWN)
   {
      gPan += (double)( x - gMouseX);
      gMouseX = x;

      gTilt += (double)( y - gMouseY);
      gMouseY = y;
   }
}

int main(int argc, char* argv[])
{
   if ( argc < 3)
   {
      printf ("Usage:\n");
      printf ("%s <3D mesh file> <camera0_image> <camera0_alphamask (optional)>\n",argv[0]);
      exit( 1);
   }

	///Split name of the file in four parts. PART0-#CAMERA-PART1-#FRAME-PART3
	string image0=string(argv[2]);
	int cameraindex = image0.rfind("camera");
	string file_part0 = image0.substr(0, cameraindex); 
	string file_part1 = image0.substr(0, cameraindex+6); 
	//Find now the frame. The numbering of the frame is 5 digits.
	int indexframe = image0.rfind("frame");
	string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
	string first_frame_str = image0.substr(indexframe+5,5);
	int first_frame=atoi(first_frame_str.c_str());
	string file_part3 = image0.substr(indexframe+10);

	//Read the images.

	string image_path;
	stringstream ss;

	for(int i = 0; i != 6; i++) {
		ss.str("");
		image_path.clear();
		ss << file_part1 << i << file_part2 << setfill('0')<< setw(5) << first_frame<< file_part3;
		image_path=ss.str();
		image[i] = imread(image_path, CV_LOAD_IMAGE_COLOR);
		if(image[i].data==NULL){
			cout << "Error reading image: "<< image_path << endl;
			return 1;
		}
	}
	
	cout << "All images opened succesfully." << endl;
	if(argc>3){
		gAlphamaskAvailable=true;
		///Split name of the file in two parts. PART0-#CAMERA-PART1
		string mask0=string(argv[3]);
		cameraindex = mask0.rfind("camera");
		file_part0 = mask0.substr(0, cameraindex); 
		file_part1 = mask0.substr(cameraindex+7);
			
	
		//Read the masks

		image_path;
		ss;

		for(int i = 0; i != 6; i++) {
			ss.str("");
			image_path.clear();
			ss << file_part0 << "camera" << i << file_part1;
			image_path=ss.str();
			mask[i] = imread(image_path, CV_LOAD_IMAGE_GRAYSCALE);
			if(mask[i].data==NULL){
				cout << "Error reading image: "<< image_path << endl;
				return 1;
			}
		}	
		cout << "All masks opened succesfully." << endl;
	}



	//Standard OpenGL initrialization
   glutInit( &argc, argv );
   glutInitWindowSize( 800, 600 );
   glutInitDisplayMode ( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
   glutCreateWindow ( "Ladybug Virtual Environment" );

   if (!read_3d_mesh( argv[1])) 
   {
       return 0;
   }

	//Callbaks?
   glutReshapeFunc( resize );
   glutDisplayFunc( display );
   glutMouseFunc( mouse );
   glutMotionFunc( motion );
   glutIdleFunc( idle );
   //CaptureViewPort();
   glutMainLoop( );

   for ( int c = 0; c < 6; c++)
   {
      delete [] gTable[c];
   }

   return 0;
}
