#include "viewer_core.hpp"


using namespace std; 
using namespace cv; 
namespace enc = sensor_msgs::image_encodings;

bool gInitialized = false;
//bool texturesUpdated= false;
GLuint gTextures[ 6]; //Define global variables where texture are stored
double gValidTextureWidth;
double gValidTextureHeight;
int gCols, gRows;
double *gTable[ 6];
bool gAlphamaskAvailable = false;
double gPan = 0.0, gTilt = 0.0;
int gMouseState = GLUT_UP;
int gMouseX, gMouseY;
char *gAlphamaskFilePrefix;

//Object with all ROS stuff
VRViewer *viewer;


//Mat image[6]; // Images contained here.
Mat mask[6]; // Images contained here.


void outputGlError( string pszLabel )
{
   GLenum errorno = glGetError();
   
   if ( errorno != GL_NO_ERROR )
   {
      printf( 
         "%s had error: #(%d) %s\r\n", 
         pszLabel.c_str(), 
         errorno, 
         gluErrorString( errorno ) );
   }
}

int get_minimum_power_of_two( int in)
{
   int i = 1;
   while ( i < in){
      i *= 2;
   }
   return i;
}

bool read_3d_mesh( string mesh_file_path)
{
   FILE *fp = fopen( mesh_file_path.c_str(), "r");
   if ( fp == NULL){
      printf( "Can't read 3D mesh file: %s\n", mesh_file_path.c_str());
      return false;
   }

   if ( fscanf( fp, "cols %d rows %d\n", &gCols, &gRows) != 2){
      printf( "Can't read cols/rows in 3d mesh file.\n");
	  fclose( fp);
      return false;
   }

   for ( int c = 0; c < 6; c++)
   {
      gTable[ c] = new double[ gCols * gRows * 3];

      for ( int iRow = 0; iRow < gRows; iRow++ )
      {
         for ( int iCol = 0; iCol < gCols; iCol++ )
         {
            double x, y, z;
            if ( fscanf( fp, "%lf, %lf, %lf", &x, &y, &z) != 3){
               printf( "Can't read grid data in 3d mesh file.\n");
               return false;
            }
            gTable[c][ ( iRow * gCols + iCol) * 3 + 0] = x;
            gTable[c][ ( iRow * gCols + iCol) * 3 + 1] = y;
            gTable[c][ ( iRow * gCols + iCol) * 3 + 2] = z;
         }
      }
   }

   fclose( fp);

   return true;
}

void initialize( void)
{   

   int width, height;

	width=1616;
	height=1232;

   glGenTextures( 6, gTextures); //generate texture names. It is guaranteed that none of the returned names was in use immediately before the call to glGenTextures.
   outputGlError( "initialize glGenTextures" );

   for ( int cam = 0; cam < 6; cam++)
   {
      glBindTexture( GL_TEXTURE_2D, gTextures[ cam]);// Now we work with this texture
      // Texture size is set to the minimum of power of two which can contain the PPM
      // so that this program works on lower OpenGL versions.
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );


      int texture_width = get_minimum_power_of_two( width);
      int texture_height = get_minimum_power_of_two( height);
      gValidTextureWidth = (double)width/texture_width;
      gValidTextureHeight = (double)height/texture_height;

      // copy PPM image pixels to the valid region of the texture.
      unsigned char *texture_buffer = new unsigned char[ texture_width * texture_height * 4];
      for ( int y = 0; y < height; y++){
         for ( int x = 0; x < width; x++){
				//cout << (int) image[cam].ptr<unsigned char>(y,x)[0] << endl;
            texture_buffer[ ( y * texture_width + x) * 4 + 0] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[0]; // B
            texture_buffer[ ( y * texture_width + x) * 4 + 1] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[1]; // G
            texture_buffer[ ( y * texture_width + x) * 4 + 2] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[2]; // R

				if( gAlphamaskAvailable){
        	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) mask[cam].ptr<unsigned char>(y,x)[0]; 
				}
				else{
        	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) 1; // A
				}
         }
      }

      // transfer the RGB buffer to graphics card.
      glTexImage2D(
         GL_TEXTURE_2D, 
         0, 
         GL_RGBA8, 
         texture_width, 
         texture_height, 
         0, 
         GL_BGRA_EXT, 
         GL_UNSIGNED_BYTE,
         texture_buffer );
      outputGlError( "initialize glTexImage2d()" );    

      // now the RGB texture is transferred to graphics, we won't need these.
      delete []texture_buffer;

      glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
      glEnable( GL_TEXTURE_2D ); //Enable two dimensional texturing
      glShadeModel( GL_FLAT );

      if ( gAlphamaskAvailable){
         glEnable( GL_BLEND );
         glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
      }
   }
}
void updateTextures(void){


   for ( int cam = 0; cam < 6; cam++)
   {
		glBindTexture(GL_TEXTURE_2D, gTextures[cam]);

		int width, height;

		width=1616;
		height=1232;

		int texture_width = get_minimum_power_of_two( width);
		int texture_height = get_minimum_power_of_two( height);
		gValidTextureWidth = (double)width/texture_width;
		gValidTextureHeight = (double)height/texture_height;

		// copy image pixels to the valid region of the texture.
		unsigned char *texture_buffer = new unsigned char[ texture_width * texture_height * 4];
		for ( int y = 0; y < height; y++){
		   for ( int x = 0; x < width; x++){

		      texture_buffer[ ( y * texture_width + x) * 4 + 0] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[0]; // B
		      texture_buffer[ ( y * texture_width + x) * 4 + 1] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[1]; // G
		      texture_buffer[ ( y * texture_width + x) * 4 + 2] = (unsigned char) viewer->image_[cam].ptr<unsigned char>(y,x)[2]; // R

				if( gAlphamaskAvailable){
		  	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) mask[cam].ptr<unsigned char>(y,x)[0]; 
				}
				else{
		  	   	texture_buffer[ ( y * texture_width + x) * 4 + 3] = (unsigned char) 1; // A
				}
		   }
		}

		// transfer the RGB buffer to graphics card.

		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texture_width, texture_height, GL_BGRA_EXT, GL_UNSIGNED_BYTE, texture_buffer);

		// now the RGB texture is transferred to graphics, we won't need these.
		delete []texture_buffer;
	}
}


//
// GLUT handlers
//

void display( void)
{
   if ( !gInitialized){
      initialize();
      gInitialized = true;
   }
	if (viewer->texturesUpdated_){
		updateTextures();
		viewer->texturesUpdated_=false;
	}

   glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
   glClearDepth(1.0);
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   glRotated( -gTilt, 1.0, 0.0, 0.0); // tilt the view
   glRotated( -gPan, 0.0, 1.0, 0.0); // pan the view
   glRotated( -90.0, 1.0, 0.0, 0.0); // convert Ladybug 3D coordinate system to OpenGL's one

   glMatrixMode( GL_TEXTURE );
   glLoadIdentity();

   for ( int c = 0; c < 6; c++) // for each camera
   {
      glBindTexture( GL_TEXTURE_2D, gTextures[ c]); //Bind (associar textura)
      for ( int iRow = 0; iRow < gRows - 1; iRow++ ) // for each row
      {
         glBegin( GL_TRIANGLE_STRIP);
         for ( int iCol = 0; iCol < gCols; iCol++ ) // for each column
         {   
            double p1 = (double)iCol / ( gCols - 1) * gValidTextureWidth;
            double q1 = (double)iRow / ( gRows - 1) * gValidTextureHeight;
            double q2 = (double)( iRow + 1.0) / ( gRows - 1) * gValidTextureHeight;

            int ptr1 = iRow * gCols + iCol;
            double x1 = gTable[c][ ptr1 * 3 + 0];
            double y1 = gTable[c][ ptr1 * 3 + 1];
            double z1 = gTable[c][ ptr1 * 3 + 2];

            int ptr2 = ( iRow + 1) * gCols + iCol;
            double x2 = gTable[c][ ptr2 * 3 + 0];
            double y2 = gTable[c][ ptr2 * 3 + 1];
            double z2 = gTable[c][ ptr2 * 3 + 2];

            glTexCoord2d( p1, q1);
            glVertex3d( x1, y1, z1);
                              
            glTexCoord2d( p1, q2);
            glVertex3d( x2, y2, z2);
         }
         glEnd();
      }      
   }

   glutSwapBuffers();
}

void resize( int w, int h )
{
   glViewport(0, 0, w, h);
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 60.0, (GLfloat)w/(GLfloat)h, 0.1, 100.0 );
}

void mouse( int button, int state, int x, int y )
{
   gMouseState = state;
   if ( gMouseState == GLUT_DOWN){
      gMouseX = x;
      gMouseY = y;
   }
}

void motion( int x, int y )
{
   if ( gMouseState == GLUT_DOWN)
   {
      gPan += (double)( x - gMouseX);
      gMouseX = x;

      gTilt += (double)( y - gMouseY);
      gMouseY = y;
   }
}


void idle( void)
{
   glutPostRedisplay();
	ros::spinOnce();
}

int main(int argc, char **argv)
{
  // Set up ROS.
  ros::init(argc, argv, "VRViewer");

  // Declare variables that can be modified by launch file or command line.
  int rate;
  string topic, mesh_file, alpha_mask;

  // Initialize node parameters from launch file or command line.
  // Use a private node handle so that multiple instances of the node can be run simultaneously
  // while using different parameters.
  ros::NodeHandle private_node_handle_("~");
  private_node_handle_.param("rate", rate, int(40));
  private_node_handle_.param("topic", topic, string("/ladybug_camera/camera0/image_raw"));
  private_node_handle_.param("mesh_file", mesh_file, string("/home/jep/Dropbox/ladybug_workspace/meshes/3d_mesh_20_air.txt"));
  private_node_handle_.param("alpha_mask", alpha_mask, string("/home/jep/Dropbox/ladybug_workspace/masks/mask_camera0_radius20.jpg"));


  // Create a new VRViewer object.
   viewer = new VRViewer( topic, mesh_file, alpha_mask);

	//===============LOAD ALPHA MASK====================
	gAlphamaskAvailable=false;

	int cameraindex;
	string file_part0, file_part1;

	///Split name of the file in two parts. PART0-#CAMERA-PART1
	string mask0=string(alpha_mask.c_str());
	cameraindex = mask0.rfind("camera");
	file_part0 = mask0.substr(0, cameraindex); 
	file_part1 = mask0.substr(cameraindex+7);
	

	//Read the masks

	string image_path;
	stringstream ss;

	for(int i = 0; i != 6; i++) {
		ss.str("");
		image_path.clear();
		ss << file_part0 << "camera" << i << file_part1;
		image_path=ss.str();
		mask[i] = imread(image_path, CV_LOAD_IMAGE_GRAYSCALE);
		if(mask[i].data==NULL){
			cout << "Error reading image: "<< image_path << endl;
			return 1;
		}
	}
	gAlphamaskAvailable=true;	
	cout << "All masks opened succesfully." << endl;


	//===================INITIALIZE OPENGL===============
/*
	for (int i=0 ; i<6 ; i++){
		image[i]=Mat::zeros(1232,1616,CV_8UC1);
	}
*/
	//Standard OpenGL initrialization
   glutInit( &argc, argv );
   glutInitWindowSize( 800, 600 );
   glutInitDisplayMode ( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
   glutCreateWindow ( "Ladybug Virtual Environment" );

   if (!read_3d_mesh( mesh_file )) 
   {
       return 1;
   }
	else{
		cout << "Mesh file opened succesfuly" << endl;
	}

	//Callbaks
   glutReshapeFunc( resize );
   glutDisplayFunc( display );
   glutMouseFunc( mouse );
   glutMotionFunc( motion );
 	glutIdleFunc( idle);
   glutMainLoop( );

   for ( int c = 0; c < 6; c++)
   {
      delete [] gTable[c];
   }

  return 0;
} // end main()
