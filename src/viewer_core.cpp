#include "viewer_core.hpp"

namespace enc = sensor_msgs::image_encodings;

VRViewer::VRViewer(string topic,string  mesh_file, string aplha_mash)
{

  // Create a subscriber.
  // Name the topic, message queue, callback function with class name, and object containing callback function.
  camera0_sub_ = nh_.subscribe("/ladybug_camera/camera0/image_raw", 1, &VRViewer::camera0Callback, this);
  camera1_sub_ = nh_.subscribe("/ladybug_camera/camera1/image_raw", 1, &VRViewer::camera1Callback, this);
  camera2_sub_ = nh_.subscribe("/ladybug_camera/camera2/image_raw", 1, &VRViewer::camera2Callback, this);
  camera3_sub_ = nh_.subscribe("/ladybug_camera/camera3/image_raw", 1, &VRViewer::camera3Callback, this);
  camera4_sub_ = nh_.subscribe("/ladybug_camera/camera4/image_raw", 1, &VRViewer::camera4Callback, this);
  camera5_sub_ = nh_.subscribe("/ladybug_camera/camera5/image_raw", 1, &VRViewer::camera5Callback, this);

  // Tell ROS how fast to run this node.
  //r_(rate);


	//Initialize images to 0
	for (int i=0 ; i<6 ; i++){
		image_[i]=cv::Mat::zeros(1232,1616,CV_8UC1);
	}

	texturesUpdated_ = false;

}

VRViewer::~VRViewer()
{
}

void VRViewer::camera0Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[0], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
void VRViewer::camera1Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[1], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
void VRViewer::camera2Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[2], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
void VRViewer::camera3Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[3], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
void VRViewer::camera4Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[4], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
void VRViewer::camera5Callback(const sensor_msgs::ImagePtr &msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	cv_ptr = cv_bridge::toCvCopy(msg, enc::BAYER_RGGB8);

	cvtColor(cv_ptr->image, image_[5], CV_BayerBG2BGR);

	texturesUpdated_ = true;
} 
