#ifndef VIEWER_CORE_H
#define VIEWER_CORE_H

#include <iostream>     // std::cout, std::endl
#include <iomanip>      // std::setfill, std::setw

#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <sstream>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ROS includes.
#include <ros/ros.h>
#include <ros/time.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CompressedImage.h>

using namespace std;

class VRViewer
{
public:
  //! Constructor.
  VRViewer(string topic,string  mesh_file, string aplha_mash);

  //! Destructor.
  ~VRViewer();

	bool texturesUpdated_;
	cv::Mat image_[6]; //6 Latest images

private:

	void camera0Callback(const sensor_msgs::CompressedImagePtr &msg);
	void camera1Callback(const sensor_msgs::CompressedImagePtr &msg);
	void camera2Callback(const sensor_msgs::CompressedImagePtr &msg);
	void camera3Callback(const sensor_msgs::CompressedImagePtr &msg);
	void camera4Callback(const sensor_msgs::CompressedImagePtr &msg);
	void camera5Callback(const sensor_msgs::CompressedImagePtr &msg);


	ros::NodeHandle nh_;
	ros::Subscriber camera0_sub_;
	ros::Subscriber camera1_sub_;
	ros::Subscriber camera2_sub_;
	ros::Subscriber camera3_sub_;
	ros::Subscriber camera4_sub_;
	ros::Subscriber camera5_sub_;
	//ros::Rate r_;

};

#endif
